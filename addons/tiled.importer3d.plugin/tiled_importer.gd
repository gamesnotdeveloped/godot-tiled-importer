tool
extends EditorImportPlugin

enum Preset {MULTIMESH, SINGLE, GRIDMAP}

const TILE_FLIP_FLAGS_ROTATE = {
    0b00: 0,
    0b01: 90,
    0b10: 180,
    0b11: 270,
}

const PresetNames = [
    "MultiMesh",
    "Single objects",
    "GridMap"
]

func _get_recognized_extensions():
    return ["tmx"]

func get_recognized_extensions():
    return _get_recognized_extensions()

func _get_resource_type():
    return "PackedScene"

func get_resource_type():
    return _get_resource_type()

func _get_importer_name():
    return "tiled.importer3d.plugin"

func get_importer_name():
    return _get_importer_name()

func _get_import_order() -> int:
    return 100

func _get_visible_name():
    return "TiledImporter3D"

func get_visible_name():
    return _get_visible_name()
    
func _get_save_extension():
    return "tscn"

func get_save_extension():
    return _get_save_extension()

func _get_preset_count():
    return 3

func get_preset_count():
    return _get_preset_count()

func _get_priority():
    return 1

func _get_preset_name(i):
    return PresetNames[i]

func get_preset_name(i):
    return _get_preset_name(i)

func _get_option_visibility(path: String, option_name: String, options: Dictionary) -> bool:
    return option_name != "mode"

func get_option_visibility(option, options):
    return _get_option_visibility("", option, options)

func _get_import_options(path: String, preset_index: int) -> Array:
    match preset_index:
        Preset.MULTIMESH:
            return [
                {"name": "sector_size", "default_value": 10},
                {"name": "visibility_range_end", "default_value": 20},
                {"name": "fade", "default_value": 1},
                {"name": "mode", "default_value": Preset.MULTIMESH},
                {"name": "scale", "default_value": 1.0}
            ]
        Preset.GRIDMAP:
            return [
                {"name": "mode", "default_value": Preset.GRIDMAP}
            ]
        _:
            return [
                {"name": "mode", "default_value": Preset.SINGLE}
            ]

func get_import_options(preset):
    return _get_import_options("", preset)


func find_node_of_type(node: Node, type):
    if node is type:
        return node
    for x in node.get_children():
        if x is type:
            return x
        else:
            var sub = find_node_of_type(x, type)
            if sub:
                return sub
    push_error("Node " + str(node) + " has no "+String(type))
    return null

func find_meshinstance(node: Node) -> MeshInstance:
    return find_node_of_type(node, MeshInstance)

func find_collisionshape(node: Node) -> CollisionShape:
    return find_node_of_type(node, CollisionShape)

func set_owner_recursive(node, new_owner):
    node.owner = new_owner
    if node.get_child_count():
        for kid in node.get_children():
            set_owner_recursive(kid, new_owner)

func get_tile_rotation_deg(tile_id):
    var flags = (tile_id >> 27) & 15
    var flip_h = bool(flags & 8)
    var flip_v = bool(flags & 4)
    return TILE_FLIP_FLAGS_ROTATE[flags >> 2]

func rotate_basis_for_tile(basis, tile_id):
    return basis.rotated(
        Vector3.UP,
        deg2rad(get_tile_rotation_deg(tile_id))
    )

func parse_tmx(file: File, options:Dictionary):
    var p = XMLParser.new()
    var map = {}
    var layer = {}
    var objects = []
    var data = []
    var sector_size = options.get("sector_size")
    var multimeshes = {}

    var err

    p.open_buffer(file.get_buffer(file.get_len()))
    err = p.read()
    
    while err == OK:
        var n = p.get_node_name()
        match n:
            "map":
                for i in range(0, p.get_attribute_count(), 1):
                    var an = p.get_attribute_name(i)
                    var av = p.get_attribute_value(i)
                    match an:
                        "width", "height", "tileheight", "tilewidth":
                            map[an] = av
            "layer":
                for i in range(0, p.get_attribute_count(), 1):
                    var an = p.get_attribute_name(i)
                    var av = p.get_attribute_value(i)
                    match an:
                        "name", "id", "width", "height":
                            layer[an] = av
            "data":
                if p.get_node_type() == XMLParser.NODE_ELEMENT:
                    p.read()
                    data = p.get_node_data().split(",", false)

        err = p.read()

    #print("FILE READ, MAP=", map, " LAYER=", layer, " DATA=", data)

    #var max_tiles = tiles.size()
    var x = 0
    var y = 0
    var w = layer["width"].to_int()
    var h = layer["height"].to_int()
    var mx = 0  # multimesh x
    var my = 0  # multimesh y
    var _scale = options.get("scale", 1.0)
    var _scale_vec = Vector3.ONE * _scale
    
    if options.get("mode") == Preset.MULTIMESH:
        for tile in data:
            mx = int(x / sector_size)
            my = int(y / sector_size)
            var mm_key = str(mx) + "_" + str(my)

            var tile_str = tile.strip_edges()
            var tile_id = tile_str.to_int()
            

            if tile_id > 0:
                var tile_number = (tile_id & 0x0fffffff) - 1
                
                if not multimeshes.has(tile_id):
                    multimeshes[tile_id] = {}

                if not multimeshes[tile_id].has(mm_key):
                    var _mm = TiledMultiMeshInstance.new()
                    _mm.tile = tile_number
                    _mm.tile_scale = _scale_vec
                    
                    _mm.tile_rotate_y = get_tile_rotation_deg(tile_id)
                    
                    #var _mesh = find_meshinstance(subobj)
                    #_mm.cast_shadow = _mesh.cast_shadow

                    #_mm.visibility_range_end = sector_size * 2 + 2
                    
                    #_mm.visibility_range_end = options.get("visibility_range_end")
                    #_mm.visibility_range_end_margin = 2
                    #if options.get("fade"):
                    #    _mm.visibility_range_fade_mode = GeometryInstance.VISIBILITY_RANGE_FADE_SELF
                    #else:
                    #    _mm.visibility_range_fade_mode = GeometryInstance.VISIBILITY_RANGE_FADE_DISABLED
                    
                    _mm.translation = Vector3(mx*sector_size*_scale, 0, my*sector_size*_scale)
                    _mm.multimesh = MultiMesh.new()
                    #_mm.multimesh.use_colors = false
                    #_mm.multimesh.use_custom_data = false
                    _mm.multimesh.color_format = MultiMesh.COLOR_NONE
                    _mm.multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE
                    
                    _mm.multimesh.transform_format = MultiMesh.TRANSFORM_3D
                    #_mm.multimesh.mesh = _mesh.mesh
                    _mm.name = "Tile "+str(tile_id)+" ["+str(mx)+","+str(my)+"]"
                    #var _sb = StaticBody3D.new()
                    #_mm.add_child(_sb)

                    multimeshes[tile_id][mm_key] = {
                        "node": _mm,
                        "tile": _mm.tile,
                        "id": tile_id,
                        #"offset": subobj.position,
                        "instances": []
                    }
                    #objects.append(_mm)

                multimeshes[tile_id][mm_key]["instances"].append([x, y])
                #_mm.multimesh.instance_count = _mm.multimesh.instance_count+1
                #_mm.multimesh.set_instance_transform(_mm.multimesh.instance_count-1, Transform3D(_mm.basis, Vector3(x-mx*sector_size, 0, y-my*sector_size)))
                #_mm.multimesh.set_instance_transform(_mm.multimesh.instance_count-1, Transform3D(_mm.basis, Vector3(x, 0, y)))

            x += 1
            if x > w-1:
                x = 0
                y += 1

        for _tile in multimeshes.values():
            for _meta in _tile.values():
                var _mm = _meta["node"]
                
                
                _mm.multimesh.instance_count = _meta["instances"].size()
                #_mm.multimesh.visible_instance_count = int(_meta["instances"].size() / 2)
                
                for _i in _meta["instances"].size():
                    var pos = _meta["instances"][_i]
                    var _x = pos[0] - (int(pos[0] / sector_size) * sector_size)
                    var _y = pos[1] - (int(pos[1] / sector_size) * sector_size)
                    var _v = Vector3(_x, 0, _y) * _scale # + _meta["offset"]
                    #_mm.transform.basis
                    
                    var _b:Basis = _mm.transform.basis
                        
                    _mm.multimesh.set_instance_transform(_i, Transform(_b, _v))
                if _meta["instances"].size() > 0:
                    objects.append(_mm)

    if options.get("mode") == Preset.SINGLE:
        for tile in data:

            var tile_str = tile.strip_edges()
            var tile_id = tile_str.to_int()
            if tile_id > 0:
                #var subobj = tiles[tile_id-1].duplicate()
                var obj = TiledMeshInstance.new()
                #subobj.name = String(subobj.name)+" "+str(x)+"_"+str(y)
                obj.name = "Tile"+str(x)+"_"+str(y)
                obj.tile = tile_id-1
                #obj.add_child(subobj)
                obj.translation = Vector3(x, 0, y)

                objects.append(obj)
            x += 1
            if x > w-1:
                x = 0
                y += 1
                
    if options.get("mode") == Preset.GRIDMAP:
        
        var grids = {}
        
        for tile in data:
            mx = int(x / sector_size)
            my = int(y / sector_size)
            var mm_key = str(mx) + "_" + str(my)
            var g:GridMap
            
            if not grids.has(mm_key):
                g = GridMap.new()
                g.cell_scale = _scale
                g.cell_size = Vector3(_scale,_scale,_scale)
                grids[mm_key] = g
            else:
                g = grids[mm_key]

            var tile_str = tile.strip_edges()
            var tile_id = tile_str.to_int()
            
            if tile_id > 0:
                var tile_number = (tile_id & 0x0fffffff) - 1
                var orientation = rotate_basis_for_tile(Basis(), tile_id).get_orthogonal_index()
                g.set_cell_item(x, 0, y, tile_number, orientation)
            x += 1
            if x > w-1:
                x = 0
                y += 1
                
        for g in grids.values():
            objects.append(g)

    return objects


func _import(source_file, save_path, options, platform_variants, gen_files):
    var file = File.new()

    if file.open(source_file, File.READ) != OK:
        return FAILED
    print("Options: ", options)
    var scene = PackedScene.new()
    var node = TiledMap.new()
    var objects = parse_tmx(file, options)
    node.name = "TiledMap"
    for obj in objects:
        print("Adding object to scene node: ", obj)
        node.add_child(obj)
        set_owner_recursive(obj, node)
    scene.pack(node)
    # Fill the Mesh with data read in "file", left as an exercise to the reader.

    var filename = save_path + "." + _get_save_extension()
    print("TILED Saving as ", filename)
    return ResourceSaver.save(filename, scene)

func import(source_file, save_path, options, platform_variants, gen_files):
    return _import(source_file, save_path, options, platform_variants, gen_files)

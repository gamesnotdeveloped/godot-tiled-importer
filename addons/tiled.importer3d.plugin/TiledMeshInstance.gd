extends MeshInstance
class_name TiledMeshInstance

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var tile:int = -1

var tile_mesh:Mesh = null setget set_tile_mesh
var tile_transform:Mesh = null setget set_tile_transform

func set_tile_mesh(x):
    mesh = x

func set_tile_transform(x):
    transform = x

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass

tool
extends EditorPlugin

var import_plugin = null
#var tileset_import_plugin = null

func get_name():
    return "Tiled Map Importer 3D"

func _enter_tree():
    import_plugin = preload("tiled_importer.gd").new()
    #tileset_import_plugin = preload("tiled_tileset_import_plugin.gd").new()
    add_import_plugin(import_plugin)
    #add_import_plugin(tileset_import_plugin)

func _exit_tree():
    remove_import_plugin(import_plugin)
    #remove_import_plugin(tileset_import_plugin)
    import_plugin = null
    #tileset_import_plugin = null

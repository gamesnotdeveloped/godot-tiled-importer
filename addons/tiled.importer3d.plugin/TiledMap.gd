tool
extends Spatial
class_name TiledMap

var _dirty = false

export var refresh:bool = false setget set_refresh
export var tileset:MeshLibrary setget set_tileset
export var cast_shadow:bool = false setget set_cast_shadow

func set_tileset(x):
    tileset = x
    _dirty = true
    
func set_refresh(x):
    refresh = false
    _dirty = true

func set_cast_shadow(x):
    cast_shadow = x
    _dirty = true

func apply_changes(node=null):
    if not node:
        node = self
    var tileset_size = tileset.get_item_list().size() if tileset else 0
    
    for child in node.get_children():
        var _inst = null
        
        if child is TiledMeshInstance or child is TiledMultiMeshInstance:
            if tileset and child.tile > -1 and child.tile < tileset_size:
                child.tile_mesh = tileset.get_item_mesh(child.tile)
                child.tile_transform = tileset.get_item_mesh_transform(child.tile)
            else:
                child.tile_mesh = null
                child.tile_transform = Transform(Basis(), Vector3.ZERO)
            child.cast_shadow = cast_shadow
            apply_changes(child)
        elif child is GridMap:
            child.mesh_library = tileset


func _ready():
    pass

func _enter_tree():
    apply_changes()

func _process(delta):
    if Engine.editor_hint:
        if _dirty:
            _dirty = false
            apply_changes()
            

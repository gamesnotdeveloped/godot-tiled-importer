tool
extends MultiMeshInstance
class_name TiledMultiMeshInstance

export var tile:int = -1
export var tile_rotate_y:float = 0.0
export var tile_scale:Vector3 = Vector3.ONE

var tile_mesh:Mesh = null setget set_tile_mesh
var tile_transform:Mesh = null setget set_tile_transform

func set_tile_mesh(x):
    multimesh.mesh = x
    

func set_tile_transform(x):
    var nb = x.basis.rotated(Vector3.UP, deg2rad(tile_rotate_y)).scaled(tile_scale)
    print(nb, " ", tile_scale)
    for i in range(multimesh.instance_count):
        var t = multimesh.get_instance_transform(i)
        var nt = Transform(nb, t.origin + x.origin)
        multimesh.set_instance_transform(i, nt)
        
func _ready():
    pass # Replace with function body.
